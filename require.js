(()=> {
    const cache = {

    }
  
            
    const data = {
        exports: {}
    };

    const api = {
        get: function(path) {
            const http = new XMLHttpRequest();
            http.open('GET', path, false);
            http.send()
            return http;
        }
    }
    
    function require(path) {
        let res = api.get(path);
        const {response: file, responseURL: url} = res;
            if (url in cache) {
                module.exports = cache[url];
                return;
            }
            cache[url] = file;
            const anonimus = new Function('module', 'exports', file);
            anonimus(data, data.exports);
        return data.exports
    }
  
    require.cache = cache;
  
    window.require = require;
  })()

  